using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api_2.Context;
using tech_test_payment_api_2.Entities;
using tech_test_payment_api_2.Models;

namespace tech_test_payment_api_2.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly SaleContext _context;

        public SaleController(SaleContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult SaleRegistration(SaleAndSalesman saleAndSalesman)
        {
            Sale sale = saleAndSalesman.Sale;
            Salesman salesman = saleAndSalesman.Salesman;

            if (sale.SalesList == "") {
                return BadRequest();
            }

            Salesman salesmanDB = _context.Salesmans.Find(salesman.Id);

            if (salesmanDB == null) {
                _context.Salesmans.Add(salesman);
            }

            sale.Status = SaleStatusEnum.WaitingPayment;
            _context.Sales.Add(sale);
            _context.SaveChanges();

            return Ok(sale);
        }

        [HttpGet("{id}")]
        public IActionResult FindSaleById(int id)
        {
            Sale sale = _context.Sales.Find(id);

            if (sale == null) {
                return NotFound();
            }

            return Ok(sale);
        }

        [HttpPut("{id}")]
        public IActionResult SaleStatusUpdate(int id, string status)
        {
            Sale sale = _context.Sales.Find(id);

            if (sale == null) {
                return NotFound();
            }

            if (sale.Status == SaleStatusEnum.WaitingPayment) {
                if (status == SaleStatusEnum.PaymentReceived.ToString()) {
                    sale.Status = SaleStatusEnum.PaymentReceived;
                } else if (status == SaleStatusEnum.Canceled.ToString()) {
                    sale.Status = SaleStatusEnum.Canceled;
                } else {
                    return BadRequest();
                }
            } else if (sale.Status == SaleStatusEnum.PaymentReceived) {
                if (status == SaleStatusEnum.InTransport.ToString()) {
                    sale.Status = SaleStatusEnum.InTransport;
                } else if (status == SaleStatusEnum.Canceled.ToString()) {
                    sale.Status = SaleStatusEnum.Canceled;
                } else {
                    return BadRequest();
                }
            } else if (sale.Status == SaleStatusEnum.InTransport) {
                if (status == SaleStatusEnum.Received.ToString()) {
                    sale.Status = SaleStatusEnum.Received;
                }
            } else {
                return BadRequest();
            }

            _context.Sales.Update(sale);
            _context.SaveChanges();

            return Ok(sale);
        }
    }
}