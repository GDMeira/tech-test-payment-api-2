using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api_2.Entities;

namespace tech_test_payment_api_2.Context
{
    public class SaleContext : DbContext
    {
        public SaleContext(DbContextOptions<SaleContext> options) : base(options)
        {

        }

        public DbSet<Sale> Sales { get; set;}

        public DbSet<Salesman> Salesmans { get; set;}
    }
}