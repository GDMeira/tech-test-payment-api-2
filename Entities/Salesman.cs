using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api_2.Entities
{
    public class Salesman
    {
        public int Id { get; set; }

        public string Cpf { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }
        
    }
}