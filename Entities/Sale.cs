using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api_2.Entities
{
    public class Sale
    {
        public int Id { get; set; }

        public int SalemanId { get; set; }

        public DateTime Date { get; set; }

        public string SalesList { get; set; }

        public SaleStatusEnum Status { get; set; }

    }
}