namespace tech_test_payment_api_2.Entities
{
    public enum SaleStatusEnum
    {
        WaitingPayment,
        PaymentReceived,
        InTransport,

        Received,

        Canceled
    }
}