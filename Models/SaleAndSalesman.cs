using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api_2.Entities;

namespace tech_test_payment_api_2.Models
{
    public class SaleAndSalesman
    {

        public SaleAndSalesman(Sale sale, Salesman salesman) 
        {
            Sale = sale;
            Salesman = salesman;
   
        }
                public Sale Sale { get; set; }

        public Salesman Salesman { get; set; }
    }
}